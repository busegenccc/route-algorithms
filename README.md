# README #

Multi drone çarpışmalarını önlemek için güzergah planlamalarını 
enine ve boyuna arama ile gerçeklediğim uygulama için App klasörünün altına gidebilirsiniz.
Çalıştırabilmek için QT kütüphanesine ihtiyaç duymaktasınız.
Ekranı başlangıç matrisine göre doldurmaktadır.Başlangıç matrisi için board.matrix'in içeriğini değiştirebilirsiniz.
P başlangıç
S ara noktalar
B engeller
F bitiş noktası

#Djikstra algoritması için Djikstra.java
#BFS algoritması için BFS.java
#DFS algoritması için DFS.java

### Bu Repository ne için? ###

KTÜ Bilgisayar Mühendisliği Optimizasyon ödevlerinden 
Multi Dron çarpışmalarının önlenmesini kod olarak inceleyebilmek ve görselleyebilmek için.

Feature: 
DFS ve A* algoritmalari eklenecek.
Farklı başlangıç noktalarından birden fazla dron ile başlatılabilir olacak.
2B çalışmadan 3B boyutlu çalışmaya irtifa değişkeni ekleyerek geçilecek.